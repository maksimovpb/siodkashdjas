<?php

namespace App\Service\Mqtt;

use App\Jobs\ProcessMqttMessage;
use App\Models\MqttMessage;
use PhpMqtt\Client\Contracts\MqttClient;
use PhpMqtt\Client\Facades\MQTT;

class MqttSubscriber implements MqttSubscriberInterface
{
    private MqttClient $client;

    public function __construct()
    {
        $this->client = MQTT::connection();
    }

    /**
     * @param string $topic
     *
     * @return void
     */
    public function subscriber(string $topic): void
    {
        $this->client->subscribe($topic, function (string $topic, string $message) {
            $mqttMessage = new MqttMessage($topic, $message);
            ProcessMqttMessage::dispatch($mqttMessage);
        }, 2);

        $this->client->loop(true);
    }
}
