<?php

namespace App\Service\Mqtt;

interface MqttSubscriberInterface
{
    /**
     * @param string $topic
     *
     * @return void
     */
    public function subscriber(string $topic): void;
}
