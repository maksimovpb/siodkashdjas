<?php

namespace App\Service\Filter;

use App\Models\MqttMessage;

interface TopicFilterInterface
{
    public function filterTopic(MqttMessage $mqttMessage): ?MqttMessage;
}
