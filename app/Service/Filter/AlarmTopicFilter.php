<?php

namespace App\Service\Filter;

use App\Models\MqttMessage;

final class AlarmTopicFilter implements TopicFilterInterface
{
    private array $allowedTopics = [
        'sos',
        'low-battery',
        'blood-pressure',
        'heart-beat',
    ];

    public function filterTopic(MqttMessage $mqttMessage): ?MqttMessage
    {
        foreach ($this->allowedTopics as $allowedTopic) {
            if (str_contains($mqttMessage->topic, $allowedTopic)) {
                return $mqttMessage;
            }
        }

        return null;
    }
}
