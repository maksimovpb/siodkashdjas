<?php

namespace App\Service\Amqp;

use PhpAmqpLib\Message\AMQPMessage;

interface AmqpPublisherInterface
{
    public function publish(AMQPMessage $message): void;
}
