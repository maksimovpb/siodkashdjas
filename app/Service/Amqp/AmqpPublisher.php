<?php

namespace App\Service\Amqp;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class AmqpPublisher implements AmqpPublisherInterface
{
    private AMQPChannel $channel;

    private array $amqpConfig;

    public function __construct()
    {
        $this->amqpConfig = config('queue.connections.amqp');
        $connection = new AMQPConnection(
            $this->amqpConfig['host'],
            $this->amqpConfig['port'],
            $this->amqpConfig['user'],
            $this->amqpConfig['password'],
            $this->amqpConfig['vhost'],
        );

        $this->channel = $connection->channel();
        $this->channel->queue_declare(
            $this->amqpConfig['queue'],
            false,
            true,
            false,
            false
        );

        $this->channel->exchange_declare(
            $this->amqpConfig['exchange_name'],
            $this->amqpConfig['exchange_type'],
            false,
            true,
            false
        );

        $this->channel->queue_bind(
            $this->amqpConfig['queue'],
            $this->amqpConfig['exchange_name'],
        );

        register_shutdown_function($this->onShutdown($this->channel, $connection));
    }

    public function publish(AMQPMessage $message): void
    {
        $this->channel->basic_publish($message, $this->amqpConfig['exchange_name']);
    }

    private function onShutdown(AMQPChannel $ch, AMQPConnection $conn): callable
    {
        return function() use ($ch, $conn) {
            $ch->close();
            $conn->close();
        };
    }
}
