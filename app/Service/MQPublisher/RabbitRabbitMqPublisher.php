<?php

namespace App\Service\MQPublisher;

use App\Models\MqttMessage;
use App\Service\Amqp\AmqpPublisherInterface;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitRabbitMqPublisher implements RabbitMqPublisherInterface
{
    public function __construct(protected AmqpPublisherInterface $amqpClient) {}

    public function publish(MqttMessage $mqttMessage): void
    {
        $msg = new AMQPMessage($mqttMessage->message, ['content_type' => 'text/plain', 'delivery_mode' => 2]);
        $this->amqpClient->publish($msg);
    }
}
