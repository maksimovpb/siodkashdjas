<?php

namespace App\Service\MQPublisher;

use App\Models\MqttMessage;

interface RabbitMqPublisherInterface
{
    public function publish(MqttMessage $mqttMessage): void;
}
