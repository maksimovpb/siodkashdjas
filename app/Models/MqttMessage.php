<?php

namespace App\Models;

class MqttMessage
{
    public function __construct(public string $topic, public string $message) {}
}
