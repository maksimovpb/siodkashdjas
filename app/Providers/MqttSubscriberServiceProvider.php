<?php

namespace App\Providers;

use App\Service\Mqtt\MqttSubscriber;
use App\Service\Mqtt\MqttSubscriberInterface;
use Illuminate\Support\ServiceProvider;

class MqttSubscriberServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MqttSubscriberInterface::class, function () {
            return new MqttSubscriber();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
