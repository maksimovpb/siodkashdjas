<?php

namespace App\Providers;

use App\Service\Amqp\AmqpPublisher;
use App\Service\MQPublisher\RabbitRabbitMqPublisher;
use App\Service\MQPublisher\RabbitMqPublisherInterface;
use Illuminate\Support\ServiceProvider;

class MessagePublisherServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RabbitMqPublisherInterface::class, function () {
            return new AmqpPublisher();
        });

        $this->app->singleton(RabbitMqPublisherInterface::class, function ($app) {
            return new RabbitRabbitMqPublisher($app->make(RabbitMqPublisherInterface::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
