<?php

namespace App\Providers;

use App\Service\Amqp\AmqpPublisher;
use App\Service\Amqp\AmqpPublisherInterface;
use App\Service\Filter\AlarmTopicFilter;
use App\Service\Filter\TopicFilterInterface;
use App\Service\MQPublisher\RabbitRabbitMqPublisher;
use App\Service\MQPublisher\RabbitMqPublisherInterface;
use App\Service\Mqtt\MqttSubscriber;
use App\Service\Mqtt\MqttSubscriberInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MqttSubscriberInterface::class, MqttSubscriber::class);

        $this->app->bind(RabbitMqPublisherInterface::class, RabbitRabbitMqPublisher::class);

        $this->app->bind(AmqpPublisherInterface::class, AmqpPublisher::class);

        $this->app->singleton(TopicFilterInterface::class, AlarmTopicFilter::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
