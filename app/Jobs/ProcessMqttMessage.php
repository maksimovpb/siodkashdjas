<?php

namespace App\Jobs;

use App\Models\MqttMessage;
use App\Service\Filter\TopicFilterInterface;
use App\Service\MQPublisher\RabbitMqPublisherInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessMqttMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected MqttMessage $mqttMessage) {}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(RabbitMqPublisherInterface $publisher, TopicFilterInterface $topicFilter)
    {
        echo json_encode($this->mqttMessage) . PHP_EOL; // todo  :: remove this line after testing

        $mqttMessage = $topicFilter->filterTopic($this->mqttMessage);

        if ($mqttMessage) {
            $publisher->publish($this->mqttMessage);
        }
    }
}
