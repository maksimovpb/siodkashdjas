<?php

namespace App\Console\Commands;

use App\Service\Mqtt\MqttSubscriberInterface;
use Illuminate\Console\Command;

class MqttStreamListenerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mqtt:subscribe {topic}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribes to given topic and listens it.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(MqttSubscriberInterface $mqttSubscriber)
    {
        try {
            $mqttSubscriber->subscriber($this->argument('topic'));

            return 0;
        } catch (\Throwable $exception) {
            $this->error($exception->getMessage());

            return 1;
        }
    }
}
