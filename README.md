# MQTT Streamer

## About MQTT Streamer

Re-streams data from MQTT topics to the message bus.

1. Subscriber to the given topic.
2. Filters messages.
3. Publishes filtered messages to the Message Bus(RabbitMQ).

### Alarm message structure
```json
{
    "deviceId": "Any device id or MAC address",
    "signalType": "SOS|low-battery|blood-pressure|heart-beat|etc..."
}
```
